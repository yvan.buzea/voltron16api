const express = require('express');

const app = express();
const port = process.env.PORT || 3016;

app.use(express.json())

require('./routes/authors')(app, 'authors');
require('./routes/books')(app, 'books');
require('./routes/book_items')(app, 'book_items');
require('./routes/genres')(app, 'genres');
require('./routes/loans')(app, 'loans');
require('./routes/localisations')(app, 'localisations');
require('./routes/members')(app, 'members');
require('./routes/publishing_houses')(app, 'publishing_houses');
require('./routes/roles')(app, 'roles');

app.listen(port, () => {
    console.log('Server listening on port ' + port )
})