const database = require('../firestore');

const collection = database.collection("book_items");

let getAll = async function getAll() {
    const coll = await collection.get();

    return coll.docs.map(doc => {
        return {
            id: doc.id,
            ...doc.data()
        }
    })
}

let getById = async function getById(id) {
    const coll = collection.doc(id);
    const doc = await coll.get();

    return {
        id : doc.id,
        ...doc.data()
    }
}

let create = async function create(data) {
	if(data.barcode && data.book_id) {
        await collection.doc().set({
            barcode  : data.barcode ,
            book_id : data.book_id,
            localisation_id : data.localisation_id || null,
            arrival_date : data.arrival_date || null,
            departure_date : data.departure_date || null,
            book_item_status : data.book_item_status || null,
        })
            .then(res => {return res})
            .catch(err => console.log(err))
    } else {
            return 'A book item must have a book_id and a barcode, at least.'
    }
	return true;
}

let update = async function update(id, data) {
    collection.doc(id).update({...data})
    .then((res) => {return res})
    .catch(err => console.log(err))
}

let remove = async function remove(id) {
    collection.doc(id).delete()
    .then(() => {return 'Book item : ' + id + ' has been deleted'})
    .catch((err) => console.log(err))
}

exports.getAll = getAll;
exports.getById = getById;
exports.create = create;
exports.update = update;
exports.remove = remove;
