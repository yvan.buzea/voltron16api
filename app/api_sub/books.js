const database = require('../firestore');

const collection = database.collection("books");

let getAll = async function getAll() {
    const coll = await collection.get();

    return coll.docs.map(doc => {
        return {
            id: doc.id,
            ...doc.data()
        }
    })
}

let create = async function create(data) {
	if(data.title) {
        await collection.doc().set({
            title : data.title,
            subtitle : data.subtitle || null,
            abstract : data.abstract || null,
            pulication_date : data.publication_date || null,
            number_of_pages : data.number_of_pages || null,
            publishing_house_id : data.publishing_house_id || null,
            category_id : data.category_id || null,
            genres : data.genres || null
        })
            .then(res => {return res})
            .catch(err => console.log(err))
    } else {
            return 'A book must have a title, at least.'
    }
	return true;
}

let update = async function update(book_id, data) {
    collection.doc(book_id).update({...data})
    .then((res) => {return res})
    .catch(err => console.log(err))
}

let removeAll = async function removeAll() {
    return 'TODO'
}

let getById = async function getById(book_id) {
    const coll = collection.doc(book_id);
    const doc = await coll.get();

    return {
        id : doc.id,
        ...doc.data()
    }
}

let remove = async function remove(book_id) {
    collection.doc(book_id).delete()
    .then(() => {return 'Book : ' + book_id + ' has been deleted'})
    .catch((err) => console.log(err))
}


let getBooks_by_author = async function getBooks_by_author(author_id) {
    return 'TODO'
}

let getBooks_by_genre = async function getBooks_by_genre(genre_id) {
    return 'TODO'
}

let getBooks_by_publishing_house = async function getBooks_by_publishing_house(publishing_house_id) {
    return 'TODO'
}

let getBooks_by_category = async function getBooks_by_category(category_id) {
    const coll = await collection.get();

    temp = await Promise.all(coll.docs.map(async doc => {
        return {
            id: doc.id,
            ...doc.data()
        }
    }));

    return temp
        .filter(e => e.category_id === category_id)
}

let getBooks_by_localisation = async function getBooks_by_genre(localisation_id) {
    return 'TODO'
}

exports.getAll = getAll;
exports.getById = getById;

exports.getBooks_by_author = getBooks_by_author;
exports.getBooks_by_genre = getBooks_by_genre;
exports.getBooks_by_publishing_house = getBooks_by_publishing_house;
exports.getBooks_by_category = getBooks_by_category;
exports.getBooks_by_localisation = getBooks_by_localisation;

exports.create = create;
exports.update = update;

exports.removeAll = removeAll;
exports.remove = remove;

