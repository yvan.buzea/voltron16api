const database = require('../firestore');

const collection = database.collection("loans");

let getAll = async function getAll() {
    const coll = await collection.get();

    return coll.docs.map(doc => {
        return {
            id: doc.id,
            ...doc.data()
        }
    })
}

let getById = async function getById(id) {
    const coll = collection.doc(id);
    const doc = await coll.get();

    return {
        id : doc.id,
        ...doc.data()
    }
}

let create = async function create(data) {
	if(data.book_item_id && data.member_id) {
        await collection.doc().set({
            book_item_id : data.book_item_id,
            member_id : data.member_id,
            date_loaned : data.date_loaned || null,
            date_due : data.date_due || null,
            date_returned : data.date_returned || null,
            loan_status : data.loan_status || null,
        })
            .then(res => {return res})
            .catch(err => console.log(err))
    } else {
            return 'A loan must have a book_item_id and a member_id, at least.'
    }
	return true;
}

let update = async function update(id, data) {
    collection.doc(id).update({...data})
    .then((res) => {return res})
    .catch(err => console.log(err))
}

let remove = async function remove(id) {
    collection.doc(id).delete()
    .then(() => {return 'Loan : ' + id + ' has been deleted'})
    .catch((err) => console.log(err))
}

exports.getAll = getAll;
exports.getById = getById;
exports.create = create;
exports.update = update;
exports.remove = remove;