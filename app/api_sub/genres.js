const database = require('../firestore');

const collection = database.collection("genres");

let getAll = async function getAll() {
    const coll = await collection.get();

    return coll.docs.map(doc => {
        return {
            id: doc.id,
            ...doc.data()
        }
    })
}

let getById = async function getById(id) {
    const coll = collection.doc(id);
    const doc = await coll.get();

    return {
        id : doc.id,
        ...doc.data()
    }
}

let create = async function create(data) {
	if(data.name) {
        await collection.doc().set({
            name : data.name,
            description : data.description || null,
            begin_date : data.begin_date || null,
            end_date : data.end_date || null,
        })
            .then(res => {return res})
            .catch(err => console.log(err))
    } else {
            return 'A genre must have a name, at least.'
    }
	return true;
}

let update = async function update(id, data) {
    collection.doc(id).update({...data})
    .then((res) => {return res})
    .catch(err => console.log(err))
}

let remove = async function remove(id) {
    collection.doc(id).delete()
    .then(() => {return 'Genre : ' + id + ' has been deleted'})
    .catch((err) => console.log(err))
}

exports.getAll = getAll;
exports.getById = getById;
exports.create = create;
exports.update = update;
exports.remove = remove;

