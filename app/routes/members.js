const api = require('../api_sub/members');

module.exports = function(app, route){
    app.get('/' + route, (req, res) => {api.getAll().then(data => res.json(data)).catch(err => console.log(err))})
    app.get('/' + route + '/:id', (req, res) => {api.getById(req.params.id).then(data => res.json(data)).catch(err => console.log(err))})
    
    app.post('/' + route, (req, res) => {api.create(req.body).then(data => res.json(data)).catch(err => console.log(err))})
    
    app.put('/' + route + '/:id', (req, res) => {api.update(req.params.id, req.body).then(data => res.json(data)).catch(err => console.log(err))})
    
    app.delete('/' + route + '/:id', (req, res) => {api.delete(req.params.id).then(data => res.json(data)).catch(err => console.log(err))})
}