const database = require('../firestore');

const collection = database.collection("authors");


let getAll = async function getAll() {
    const coll = await collection.get();

    return coll.docs.map(doc => {
        return {
            id: doc.id,
            ...doc.data()
        }
    })
}

let getById = async function getById(author_id) {
    const coll = collection.doc(author_id);
    const doc = await coll.get();

    return {
        id : doc.id,
        ...doc.data()
    }
}

let create = async function create(data) {
	if(data.firstname) {
        await collection.doc().set({
            firstname : data.firstname,
            lastname : data.lastname || null,
            surname : data.surname || null,
            birth_date : data.birth_date || null,
            death_date : data.death_date || null,
            birth_place : data.birth_place || null,
            death_place : data.death_place || null,
            curriculum_vitae : data.curriculum_vitae || null
        })
            .then(res => {return res})
            .catch(err => console.log(err))
    } else {
            return 'An author must have a firstname, at least.'
    }
	return true;
}

let update = async function update(author_id, data) {
    collection.doc(author_id).update({...data})
    .then((res) => {return res})
    .catch(err => console.log(err))
}

let remove = async function remove(author_id) {
    collection.doc(author_id).delete()
    .then(() => {return 'Author : ' + author_id + ' has been deleted'})
    .catch((err) => console.log(err))
}

let removeAll = async function remove() {
    return 'TODO'
}

let getAuthors_by_genre = async function getAuthors_by_genre(genre_id) {
    return 'TODO'
}

let getAuthors_by_publishing_house = async function getAuthors_by_publishing_house(publishing_house_id) {
    return 'TODO'
}

let getAuthors_by_category = async function getAuthors_by_category(category_id) {
    const coll = await collection.get();

    temp = await Promise.all(coll.docs.map(async doc => {
        return {
            id: doc.id,
            ...doc.data()
        }
    }));

    return temp
        .filter(e => e.category_id === category_id)
}

exports.getAll = getAll;
exports.getById = getById;

exports.getAuthors_by_genre = getAuthors_by_genre;
exports.getAuthors_by_publishing_house = getAuthors_by_publishing_house;
exports.getAuthors_by_category = getAuthors_by_category;

exports.create = create;
exports.update = update;

exports.removeAll = removeAll;
exports.remove = remove;
