const api = require('../api_sub/authors');

module.exports = function(app, route){
    app.get('/' + route, (req, res) => {api.getAll().then(data => res.json(data)).catch(err => console.log(err))})
    app.get('/' + route + '/:id', (req, res) => {api.getById(req.params.id).then(data => res.json(data)).catch(err => console.log(err))})
    
    app.post('/' + route, (req, res) => {api.create(req.body).then(data => res.json(data)).catch(err => console.log(err))})
    
    app.put('/' + route + '/:id', (req, res) => {api.update(req.params.id, req.body).then(data => res.json(data)).catch(err => console.log(err))})
    
    app.delete('/' + route + '/:id', (req, res) => {api.delete(req.params.id).then(data => res.json(data)).catch(err => console.log(err))})
    
    app.get('/categories/:category_id/' + route, (req, res) => {api.getAuthors_by_category(req.params.category_id).then(data => res.json(data)).catch(err => console.log(err))})
    app.get('/genres/:genre_id/' + route, (req, res) => {api.getAuthors_by_genre(req.params.genre_id).then(data => res.json(data)).catch(err => console.log(err))})
    app.get('/publishing_houses/:publishing_house_id/' + route, (req, res) => {api.getAuthors_by_publishing_hosue(req.params.publishing_house_id).then(data => res.json(data)).catch(err => console.log(err))})

}